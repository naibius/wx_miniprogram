# 座位预约系统

#### 介绍
theseven——座位预约系统 

成员：叶杰晖、朱澎湧、王志坚、王嘉轩、庄宇峰、曹千成

座位预约系统可以为同学们提供预约图书馆座位的功能，方便同学们到图书馆自习，避免到了图书馆才发现没用座位的尴尬情况。

#### 软件架构
1.学号、手机号绑定

2.首页流量通知、信息

3.个人主页展示

4.预约座位

5.座位状态图展示


#### 安装教程

1.  打开微信app搜索'至诚座位预约'
2.  打开'至诚座位预约'小程序

#### 使用说明

1.  打开'至诚座位预约'小程序，绑定手机号、学号
2.  点击预约座位，选择预约时间即预约座位
3.  点击提交
4.  ‘我的’页面查看预约记录
5.  ‘我的’页面进行扫码签到即签退


#### 目录结构说明

```
-cloudfunctions云函数目录
-miniprogram/images项目图片目录
-miniprogram/pages项目页面目录
-miniprogram/utils公共资源目录
app.js项目入口文件
app.json项目配置文件
app.wxss全局样式文件
```

#### 演示图

- 首页

[![rsEZAH.png](https://s3.ax1x.com/2020/12/22/rsEZAH.png)](https://imgchr.com/i/rsEZAH)

- 座位预约界面

[![rsEijK.png](https://s3.ax1x.com/2020/12/22/rsEijK.png)](https://imgchr.com/i/rsEijK)

- 查看座位

[![rsEABD.png](https://s3.ax1x.com/2020/12/22/rsEABD.png)](https://imgchr.com/i/rsEABD)

- 我的页面

[![rsEm4A.png](https://s3.ax1x.com/2020/12/22/rsEm4A.png)](https://imgchr.com/i/rsEm4A)

- 我的预约

[![rsEknO.png](https://s3.ax1x.com/2020/12/22/rsEknO.png)](https://imgchr.com/i/rsEknO)

- 扫码页面

[![rsEEHe.png](https://s3.ax1x.com/2020/12/22/rsEEHe.png)](https://imgchr.com/i/rsEEHe)









