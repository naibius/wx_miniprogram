// pages/myOrder/orderMessage.js
import { formatTime, formatDate } from '../../utils/util.js';
Page({

  data: {},

  onLoad: function (options) {
    const that = this;
    //根据code获取openid等信息
    wx.login({
      //获取code
      success: function (res) {
        var code = res.code; //返回code
        var appId = 'wx0e6e1d51f7e4d437';
        var secret = '1c928e6afcc071a1627bd2c31b2a532a';
        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + code + '&grant_type=authorization_code',
          data: {},
          header: {
            'content-type': 'json'
          },
          success: function (res) {
            var code_openid = res.data.openid
            //根据云函数获取openid等信息
            wx.cloud.callFunction({
              name:"checkMyBookInfo",
              data: {
                openid: code_openid
              }
            })
            .then(res=>{
              that.setData({
                dataList:res.result.data,
                stuid:res.result.data[0].stu_id,
                endtime:res.result.data[0].bookEndTime,
              });
              console.log(res);
              var endHour =parseInt((that.data.endtime).substring(0, 2));
              var endMin=parseInt((that.data.endtime).substring(3, 5))
              that.setData({
                endHour,
                endMin
              })
              var a = that.data.dataList[0].seat_id.split("-");
              var x = parseInt(a[0]);
              var y = parseInt(a[1]);
              that.setData({
                row:x+1,
                column:y+1
              })
            })
            
          }
        })
      }
    })
  },

  onReady: function (options) {
    //获取当前时间
    var DATE = formatDate(new Date());
    this.setData({
      nowDate: DATE
    })
    var that = this;
    var onload_time = formatTime(new Date());
    var TIME = onload_time;
    var time_flag = 0;
    setInterval(function () {
      that.setData({
        TIME: formatTime(new Date())
      })
      that.setData({
        nowHour: parseInt((that.data.TIME).substring(0, 2)),
        nowMin: parseInt((that.data.TIME).substring(3, 5))
      })
      console.log(that.data.TIME);
      var hour=that.data.endHour-that.data.nowHour;
      if(that.data.nowMin<that.data.endMin){
        var min=that.data.endMin-that.data.nowMin;
        that.setData({
          msg:'距离结束还有:'+hour+'小时'+min+'分钟'
        })
      }
      else{
        var min=that.data.endMin-that.data.nowMin+60;
        hour=hour-1;
        that.setData({
          msg:'距离结束还有:'+hour+'小时'+min+'分钟'
        })
      }
    }, 1000);
  },
  onShow: function () {

    // 页面显示

  },

  onHide: function () {

    // 页面隐藏

  },

  onUnload: function () {

    // 页面关闭

  }

})