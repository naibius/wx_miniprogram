Page({
  data:{

  },
  //跳转至主页
  jumpToHome(e){
    wx.switchTab({
      url: '../home/home'
    });
  },
  jumpToHistory(e){
    wx.navigateTo({
      url: '../book_his/book_his'
    });
  },
  //获取我的预约数据
  onLoad: function(options){
    const that = this;
    //根据code获取openid等信息
    wx.login({
      //获取code
      success: function (res) {
        var code = res.code; //返回code
        var appId = 'wx0e6e1d51f7e4d437';
        var secret = '1c928e6afcc071a1627bd2c31b2a532a';
        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + code + '&grant_type=authorization_code',
          data: {},
          header: {
            'content-type': 'json'
          },
          success: function (res) {
            var code_openid = res.data.openid
            //根据云函数获取openid等信息
            wx.cloud.callFunction({
              name:"checkMyBookInfo",
              data: {
                openid: code_openid
              }
            })
            .then(res=>{
              that.setData({
                dataList:res.result.data
              })
            })
            .then(res=>{
              var a = that.data.dataList[0].seat_id.split("-");
              var x = parseInt(a[0]);
              var y = parseInt(a[1]);
              that.setData({
                row:x+1,
                column:y+1
              })
            })
          }
        })
      }
    })
    
  }
})
