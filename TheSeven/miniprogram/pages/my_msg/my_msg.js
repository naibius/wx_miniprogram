const db = wx.cloud.database()
Page({
  data: {
    flag: 0
  },
  jumpToBookRecord(e) {
    wx.navigateTo({
      url: '../book_record/book_record',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  jumpToScan(e){
    wx.navigateTo({
      url: '../scan/scan',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  jumpToInfo(e){
    wx.navigateTo({
      url: '../information/information',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  onLoad() {
    const that = this;
    //根据code获取openid等信息
    wx.login({
      //获取code
      success: function (res) {
        var code = res.code; //返回code
        var appId = 'wx0e6e1d51f7e4d437';
        var secret = '1c928e6afcc071a1627bd2c31b2a532a';
        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + code + '&grant_type=authorization_code',
          data: {},
          header: {
            'content-type': 'json'
          },
          success: function (res) {
            that.setData({
              code_openid: res.data.openid
            })
            //根据云函数获取我的预约信息
            wx.cloud.callFunction({
              name: "checkMyBookInfo",
              data: {
                openid: that.data.code_openid
              }
            }).then(res => {
              that.setData({
                flag:res.result.data.length
              })
            })
          }
        })
      }
    })
  },
})