const db = wx.cloud.database()
Page({
  data: {

  },
  //提交表单数据添加到数据库
  onLoad: function (options) {

  },
  btnSub(res) {
    var { id, phone } = res.detail.value;
    var reflag = 0; //flag为0可插入，1不可插入
    var phnflag = 0;
    var idflag = 0;
    var id_partten = /^(211)\d{6}$/;
    var phone_partten = /^1[3,4,5,7,8]\d{9}$/;
    //数据库中筛选相同手机号、学号
    wx.cloud.callFunction({
      name: "checkStuInfo",
      data: {
        insert_id: id,
        insert_phone: phone
      }
    }).then(res => {
      reflag = res.result.data.length,
        console.log('reflag: ' + reflag)
    }).then(res => {
      //判断学号是否符合规则
      if (!(id_partten.test(id))) {
        console.log('id:::' + id_partten.test(id));
        idflag = 1;
        wx.showModal({
          title: '提示',
          content: '学号不正确哦！！',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        });
      }
      //判断手机号是否符合规则
      if (!(phone_partten.test(phone))) {
        console.log('phone:::' + phone_partten.test(phone));
        phnflag = 1;
        wx.showModal({
          title: '提示',
          content: '手机号不正确哦！！',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        });
      }
      //判断学号、手机号是否重复
      if (reflag && !phnflag && !idflag) {
        wx.showModal({
          title: '提示',
          content: '学号或手机号已被使用！请重新输入！',
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        });
      }
      if (!reflag && !phnflag && !idflag) {
        db.collection("student").add({
          data: {
            id: id,
            phone: phone
          }
        }).then(res => {
          //跳转至'我的'页面
          wx.switchTab({
            url: '../my_msg/my_msg'
          });
        })
      }
    })
  },
  onShow: function () {
    if (wx.canIUse('hideHomeButton')) {
      wx.hideHomeButton()
    }
  }
})