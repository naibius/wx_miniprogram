import { formatTime, formatDate } from '../../utils/util.js';
Page({
  data: {
    color: [
      ['green', 'green', 'green', 'green', 'green'],
      ['green', 'green', 'green', 'green', 'green'],
      ['green', 'green', 'green', 'green', 'green',]
    ]
  },
  jumpToBookChair(e) {
    wx.switchTab({
      url: '../book_chair/book_chair',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  jumpToHome(e) {
    wx.switchTab({
      url: '../home/home',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  cancelBook(e) {
    const that = this;
    wx.showModal({
      title: '提示',
      content: '确认取消预约吗？',
      success: function (res) {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "removeMyBook",
            data: {
              openid: that.data.code_openid
            }
          })
          wx.switchTab({
            url: '../my_msg/my_msg',
          });
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  onTabItemTap: function (options) {
    const that = this;
    //根据code获取openid等信息
    wx.login({
      //获取code
      success: function (res) {
        var code = res.code; //返回code
        var appId = 'wx0e6e1d51f7e4d437';
        var secret = '1c928e6afcc071a1627bd2c31b2a532a';
        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + code + '&grant_type=authorization_code',
          data: {},
          header: {
            'content-type': 'json'
          },
          success: function (res) {
            that.setData({
              code_openid: res.data.openid
            })
            //根据云函数获取我的预约信息
            wx.cloud.callFunction({
              name: "checkMyBookInfo",
              data: {
                openid: that.data.code_openid
              }
            }).then(res => {
              that.setData({
                my_seat_id: res.result.data[0].seat_id,
                book_end_time: res.result.data[0].bookEndTime,
                book_start_time: res.result.data[0].bookStartTime
              })
            }).then(res => {
              console.log('结束时间：' + that.data.book_end_time);
              var a = that.data.my_seat_id.split("-");
              var x = parseInt(a[0]);
              var y = parseInt(a[1]);
              var col = "color[" + x + "][" + y + "]";
              that.setData({
                [col]: 'blue',
                hasbook: true
              })
            })
            //根据云函数获取他人预约信息
            wx.cloud.callFunction({
              name: "checkOthersBookInfo",
              data: {
                openid: that.data.code_openid,
              }
            }).then(res => {
              console.log(res);
              that.setData({
                array_length: res.result.data.length
              })
              for (var i = 0; i < that.data.array_length; i++) {
                that.setData({
                  ["others_seat_id[" + i + "]"]: res.result.data[i].seat_id,
                  ["others_book_end_time[" + i + "]"]: res.result.data[i].bookEndTime,
                  ["others_book_start_time[" + i + "]"]: res.result.data[i].bookStartTime
                })
              }
            }).then(res => {
              for (var i = 0; i < that.data.array_length; i++) {
                var a = that.data.others_seat_id[i].split("-");
                var x = parseInt(a[0]);
                var y = parseInt(a[1]);
                var col = "color[" + x + "][" + y + "]";
                var oendH = parseInt((that.data.others_book_end_time[i]).substring(0, 2));
                var oendM = parseInt((that.data.others_book_end_time[i]).substring(3, 5));
                var ostartH = parseInt((that.data.others_book_start_time[i]).substring(0, 2));
                var ostartM = parseInt((that.data.others_book_start_time[i]).substring(3, 5));
                if(that.data.currH<ostartH){
                  that.setData({
                    [col]: 'grey',
                  })
                }
                else if(that.data.currH==ostartH && that.data.currM<ostartM){
                  that.setData({
                    [col]: 'grey',
                  })
                }
                else if(that.data.currH==ostartH && that.data.currM>ostartM){
                  that.setData({
                    [col]: 'red',
                  })
                }
                else if(that.data.currH>ostartH){
                  that.setData({
                    [col]: 'red',
                  })
                }
                
              }
            })
          }
        })
      }
    })
  },
  onLoad: function () {
    var that = this;
    var onload_time1 = formatTime(new Date());
    var onload_time = onload_time1;
    var time_flag = 0;
    setInterval(function () {
      that.setData({
        onload_time: formatTime(new Date())
      })
      that.setData({
        currH: parseInt((that.data.onload_time).substring(0, 2)),
        currM: parseInt((that.data.onload_time).substring(3, 5))
      })
      // console.log(that.data.onload_time);
      if (that.data.hasbook == true && time_flag == 0) {
        var endH = parseInt((that.data.book_end_time).substring(0, 2));
        var endM = parseInt((that.data.book_end_time).substring(3, 5));
        if (endH < that.data.currH) {
          time_flag = 1;
          wx.cloud.callFunction({
            name: "removeMyBook",
            data: {
              openid: that.data.code_openid
            }
          })
        }
        else if (endH == that.data.currH) {
          if (endM <= that.data.currM) {
            time_flag = 1;
            wx.cloud.callFunction({
              name: "removeMyBook",
              data: {
                openid: that.data.code_openid
              }
            })
          }
        }
      }
    }, 1000);
  }
})