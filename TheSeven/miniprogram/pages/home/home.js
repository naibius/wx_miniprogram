const db = wx.cloud.database()
//Page Object
Page({
  data: {
    openid: ""
  },
  //options(Object)
  onLoad: function (options) {
    var that = this;
    //实时（每隔一秒）获取位置信息
    setInterval(function () {
      wx.getLocation({
        type: 'wgs84',
        success(res) {
          // console.log(res)
          that.setData({
            latitude : res.latitude,
            longitude : res.longitude,
            speed : res.speed,
            accuracy : res.accuracy
          });
          
        }
      })
    }, 1000);
  },
  onReady: function () {
    //根据code获取openid等信息
    wx.login({
      //获取code
      success: function (res) {
        var code = res.code; //返回code
        var appId = 'wx0e6e1d51f7e4d437';
        var secret = '1c928e6afcc071a1627bd2c31b2a532a';
        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session?appid=' + appId + '&secret=' + secret + '&js_code=' + code + '&grant_type=authorization_code',
          data: {},
          header: {
            'content-type': 'json'
          },
          success: function (res) {
            var code_openid = res.data.openid
            //根据云函数获取openid等信息
            wx.cloud.callFunction({
              name: "getStuInfo",
              data: {
                openid: code_openid
              }
            }).then(res => {
              if (res.result.data.length == 0) {
                wx.redirectTo({
                  url: '../bind_msg/bind_msg',
                });
              }
            })
          }
        })
      }
    })
  },
  onShow: function () {

  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {

  },
  onPageScroll: function () {

  },
  //item(index,pagePath,text)
  onTabItemTap: function (item) {

  }
});