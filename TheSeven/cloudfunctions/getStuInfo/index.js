// 云函数入口文件 用于home.js,book_record.js
const cloud = require('wx-server-sdk')

cloud.init()
const db=cloud.database();
// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection("student").where({_openid:event.openid}).get()
}