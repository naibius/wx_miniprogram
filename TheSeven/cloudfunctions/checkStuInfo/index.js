// 云函数入口文件 用于bind_msg.js
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection("student")
    .where(_.or([
      {
        id: event.insert_id
      },
      {
        phone: event.insert_phone
      }
    ])
    ).get()

}