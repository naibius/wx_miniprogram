module.exports = {formatTime,formatDate};
function formatTime(date){
    const hour = date.getHours();
    const minute = date.getMinutes();
    if(hour<10){
        if(minute<10){
            return "0"+hour+":0"+minute;
        }
        else{
            return "0"+hour+":"+minute;
        }
    }
    else{
        if(minute<10){
            return hour+":0"+minute;
        }
        else{
            return hour+":"+minute;
        }
    }
}
function formatDate(date) {
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()
    return year+"/"+month+"/"+day;
}
   
   