const db = wx.cloud.database()
//Page Object
Page({
  data: {
    openid: ""
  },
  //options(Object)
  onLoad: function (options) {
    var that = this;
    //实时（每隔一秒）获取位置信息
    setInterval(function () {
      wx.getLocation({
        type: 'wgs84',
        success(res) {
          // console.log(res)
          that.setData({
            latitude: res.latitude,
            longitude: res.longitude,
            speed: res.speed,
            accuracy: res.accuracy
          });

        }
      })
    }, 1000);
  },
  onReady: function () {
    var that = this;
    wx.cloud.callFunction({
      name: 'get',
    }).then(res => {
      console.log(res.result.openid)
        that.setData({
          code_openid: res.result.openid
        })
      //根据云函数获取openid等信息
      wx.cloud.callFunction({
        name: "getStuInfo",
        data: {
          openid: that.data.code_openid
        }
      }).then(res1 => {
        if (res1.result.data.length == 0) {
          wx.redirectTo({
            url: '../bind_msg/bind_msg',
          });
        }
      })

    })
  },
  onShow: function () {

  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },
  onShareAppMessage: function () {

  },
  onPageScroll: function () {

  },
  //item(index,pagePath,text)
  onTabItemTap: function (item) {

  }
});