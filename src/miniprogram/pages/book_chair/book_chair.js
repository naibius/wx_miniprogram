const db = wx.cloud.database();
import { formatTime, formatDate } from '../../utils/util.js';
Page({
  data: {
    focus: false,
    inputValue: '',
    startTime: '08:00',
    endTime: '',
    array: ['0.5h', '1.0h', '2.0h', '3.0h', '5.0h'],
    objectArray: [
      {
        id: 0,
        name: '0.5'
      },
      {
        id: 1,
        name: '1.0'
      },
      {
        id: 2,
        name: '2.0'
      },
      {
        id: 3,
        name: '3.0'
      }
      ,
      {
        id: 4,
        name: '5.0'
      }
    ],
    index: 0,
    flag: 1,
    temp: 0,
    seatnow: 0,
    color: [
      ['green', 'green', 'green', 'green', 'green'],
      ['green', 'green', 'green', 'green', 'green'],
      ['green', 'green', 'green', 'green', 'green',]
    ]
  },
  onLoad: function (options) {
    const that = this;
    // //获取当前时间
    var TIME = formatTime(new Date());
    that.setData({
      TIME: TIME
    })
    that.setData({
      nowHour: parseInt((that.data.TIME).substring(0, 2)),
      nowMin: parseInt((that.data.TIME).substring(3, 5))
    })
    if (that.data.nowHour < 21 && that.data.nowHour >= 8) {
      that.setData({
        firstTime: TIME,
        startTime: TIME
      });
    }
    wx.cloud.callFunction({
      name: 'get',
    }).then(res => {
      that.setData({
        code_openid: res.result.openid
      })
      //根据云函数获取Student表信息
      wx.cloud.callFunction({
        name: "getStuInfo",
        data: {
          openid: that.data.code_openid
        }
      }).then(res => {
        that.setData({
          stu_id: res.result.data[0].id
        })
      })
      //根据云函数获取我的预约信息
      wx.cloud.callFunction({
        name: "checkMyBookInfo",
        data: {
          openid: that.data.code_openid
        }
      }).then(res => {
        that.setData({
          my_seat_id: res.result.data[0].seat_id,
          temp: res.result.data.length,
          book_end_time: res.result.data[0].bookEndTime,
          book_start_time: res.result.data[0].bookStartTime
        })
      }).then(res => {
        var a = that.data.my_seat_id.split("-");
        var x = parseInt(a[0]);
        var y = parseInt(a[1]);
        var col = "color[" + x + "][" + y + "]";
        // if (a != undefined) {
        //   wx.cloud.callFunction({
        //     name: "checkSeatInfo",
        //     data: {
        //       openid: that.data.code_openid
        //     }
        //   }).then(res => {
        //       console.log(res.result.data[0].seat_x, res.result.data[0].seat_y, res.result.data[0].isCancel)
        //       that.setData({
        //         seat_x: res.result.data[0].seat_x,
        //         seat_y: res.result.data[0].seat_y,
        //         isCancel: res.result.data[0].isCancel
        //       })
            
        //   })
        // }

        that.setData({
          [col]: 'blue'
        })
        if (that.data.color[x][y] == 'blue') {
          that.setData({
            seatnow: 1
          })
        }
      })
      //根据云函数获取他人预约信息
      wx.cloud.callFunction({
        name: "checkOthersBookInfo",
        data: {
          openid: that.data.code_openid,
        }
      }).then(res => {
        
        that.setData({
          array_length: res.result.data.length
        })
        for (var i = 0; i < that.data.array_length; i++) {
          that.setData({
            ["others_seat_id[" + i + "]"]: res.result.data[i].seat_id,
            ["others_book_end_time[" + i + "]"]: res.result.data[i].bookEndTime,
            ["others_book_start_time[" + i + "]"]: res.result.data[i].bookStartTime
          })
        }
      }).then(res => {
        for (var i = 0; i < that.data.array_length; i++) {
          var a = that.data.others_seat_id[i].split("-");
          var x = parseInt(a[0]);
          var y = parseInt(a[1]);
          var col = "color[" + x + "][" + y + "]";
          var oendH = parseInt((that.data.others_book_end_time[i]).substring(0, 2));
          var oendM = parseInt((that.data.others_book_end_time[i]).substring(3, 5));
          var ostartH = parseInt((that.data.others_book_start_time[i]).substring(0, 2));
          var ostartM = parseInt((that.data.others_book_start_time[i]).substring(3, 5));
          if (that.data.nowHour < ostartH) {
            console.log(1);
            that.setData({
              [col]: 'grey',
            })
          }
          else if (that.data.nowHour == ostartH && that.data.nowMin <= ostartM) {
            console.log(11);
            that.setData({
              [col]: 'grey',
            })
          }
          else if (that.data.nowHour == ostartH && that.data.nowMin >= ostartM) {
            console.log(111);
            that.setData({
              [col]: 'red',
            })
          }
          else if (that.data.nowHour > ostartH && that.data.nowMin<oendM) {
            console.log(1111);
            that.setData({
              [col]: 'red',
            })
          }
          if(that.data.nowHour>oendH ||(that.data.nowHour==oendH && that.data.nowMin>oendM)){
            console.log(11111);
            wx.cloud.callFunction({
              name: "removeBooked",
              data: {
                openid: that.data.code_openid
              },
              success:function(){
                that.setData({
                  [col]:'green'
                })
              }
            })
          }
        }
      })
    })
  },
  bindStartTimeChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      startTime: e.detail.value
    })
  },
  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
  onReady: function (options) {
    //获取当前时间
    var DATE = formatDate(new Date());
    this.setData({
      nowDate: DATE
    })
    var that = this;
    var onload_time = formatTime(new Date());
    var TIME = onload_time;
    var time_flag = 0;
    setInterval(function () {
      that.setData({
        TIME: formatTime(new Date())
      })
      that.setData({
        nowHour: parseInt((that.data.TIME).substring(0, 2)),
        nowMin: parseInt((that.data.TIME).substring(3, 5))
      })

      if (that.data.nowHour < 21 && that.data.nowHour >= 8) {

        if (that.data.hasbook == true && time_flag == 0) {
          var endH = parseInt((that.data.book_end_time).substring(0, 2));
          var endM = parseInt((that.data.book_end_time).substring(3, 5));
          if (endH < that.data.nowHour) {
            time_flag = 1;
            wx.cloud.callFunction({
              name: "removeMyBook",
              data: {
                openid: that.data.code_openid
              }
            })
          }
          else if (endH == that.data.nowHour) {
            if (endM <= that.data.nowMin) {
              time_flag = 1;
              wx.cloud.callFunction({
                name: "removeMyBook",
                data: {
                  openid: that.data.code_openid
                }
              })
            }
          }
        }
      }
    }, 1000);
  },
  //更新endTime
  setEndTime(endHour, endMin) {
    if (parseInt(endHour) < 10) {
      if (parseInt(endMin) < 10) {
        this.setData({
          endTime: "0" + endHour + ":0" + endMin
        })
        console.log(this.data.endTime);
      }
      else {
        this.setData({
          endTime: "0" + endHour + ":" + endMin
        })
        console.log(this.data.endTime);
      }
    }
    else {
      if (parseInt(endMin) < 10) {
        this.setData({
          endTime: endHour + ":0" + endMin
        })
        console.log(this.data.endTime);
      }
      else {
        this.setData({
          endTime: endHour + ":" + endMin
        })
        console.log(this.data.endTime);
      }
    }
  },
  //提交表单数据添加到数据库
  btnSub(res) {
    var startTime = this.data.startTime;
    var totalTime = this.data.array[this.data.index];
    var startHour = startTime.substring(0, 2);
    var startMin = startTime.substring(3);
    var bookHour = totalTime.substring(0, 1);
    var bookMin = totalTime.substring(2, 3);
    var endMin = '';
    var endHour = '';
    var that = this;
    //预约时长含半小时判断
    if (bookMin == '5') {
      //预约时长加开始时间的总分钟数>=60
      if (parseInt(startMin) + 30 >= 60) {
        endMin = (parseInt(startMin) - 30).toString();
        endHour = (parseInt(startHour) + parseInt(bookHour) + 1).toString();
        this.setEndTime(endHour, endMin);
      }
      //预约时长加开始时间的总分钟数<60
      else {
        endMin = (parseInt(startMin) + 30).toString();
        endHour = (parseInt(startHour) + parseInt(bookHour)).toString();
        this.setEndTime(endHour, endMin);
      }
    }
    //预约时长不含半小时判断
    else {
      endMin = (parseInt(startMin)).toString();
      endHour = (parseInt(startHour) + parseInt(bookHour)).toString();
      this.setEndTime(endHour, endMin);
    }
    //超过预约时间范围
    if ((parseInt(endHour) == 21 && parseInt(endMin) > 29) || (parseInt(endHour) > 21)) {
      wx.showModal({
        title: '提示',
        content: '超过预约时间范围啦！！',
        success(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      });
      this.setData({
        flag: 0
      });
    }
    //未超过预约时间范围
    else {
      this.setData({
        flag: 1
      })
    }
    //判断是否符合插入条件
    if (this.data.temp == 1) {
      wx.showModal({
        title: '提示！',
        content: '一个人只能预约一个位置哦！',
      })
    }
    else {
      if (this.data.seat_id == null) {
        wx.showModal({
          title: '提示！',
          content: '还没选位置哦！',
        })
      }
      else if (this.data.flag == 1) {
        db.collection("book_record").add({
          data: {
            bookStartTime: startTime,
            bookEndTime: this.data.endTime,
            bookTotalTime: this.data.array[this.data.index],
            seat_id: this.data.seat_id,
            stu_id: this.data.stu_id
          }
        }).then(res =>{
          db.collection("seat").add({
            data: {
              seat_x:this.data.x,
              seat_y:this.data.y,
              isCancel:false
            }
          })
        })


        that.onLoad();
        //跳转页面
        wx.showModal({
          title: '恭喜！',
          content: '预约成功啦！',
          success: function (res) {

            wx.switchTab({
              url: '../my_msg/my_msg',
            });
          }
        })
      }
    }
  },
  click: function (e) {
    var th = this;
    var id = e.target.id;
    var a = id.split("-");
    var x = parseInt(a[0]);
    var y = parseInt(a[1]);
    var col = 'color[' + x + '][' + y + ']';
    var color = th.data.color[x][y];
    if (color == 'red' || color == 'grey') {
      wx.showModal({
        title: 'Warning',
        content: '这个座位有人啦！！',
        success: function (res) {
          console.log('用户点击确定')
        }
      })
    } else {
      if (th.data.seatnow == 0) { //是否选择了座位 未选
        this.setData({
          seat_id: id, //
          x: x, //这里的东西我们要用来写离座的函数，直接留下，不用再进行获取了（对此函数无意，先记着我们把选择的座位id，x，y存下了，后面讲为什么）
          y: y, //
        })
        wx.showModal({
          title: '提示',
          content: '确认选择当前座位吗？',
          success: function (res) {
            if (res.confirm) {
              th.setData({
                [col]: 'blue', //选座成功，选择的位置编程蓝色，选座状态变为1
                seatnow: 1
              })
              console.log("color" + th.data.color[x][y]);
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      } else if (color == 'green') { //已选
        wx.showModal({
          title: '提示',
          content: '您当前已经选了一个座位啦，不能再选啦！！',
          success: function (res) {
            console.log('用户点击确定')
          }
        })
      } else { //已选 且为蓝色
        wx.showModal({
          title: '提示',
          content: '这是你的座位',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    }
  },
  repick: function (e) {
    var that = this;
    var x = this.data.x; //
    var y = this.data.y; //这里我们不再获取，click函数里面我们已经存了起来，直接拿来用（这里就用到了我们上面保存的座位信息，知道为什么了吧。我们需要下标在离座的时候把颜色变回来）
    var id = this.data.seat_id; //
    var col = 'color[' + x + '][' + y + ']';
    if (that.data.seatnow == 0) {
      wx.showModal({
        title: '提示',
        content: '当前还未选择位置哦！'
      })
    }
    else {
      wx.showModal({
        title: '提示',
        content: '确定要重选吗?',
        success: function (res) {
          if (res.confirm) {
            that.setData({
              [col]: 'green',
              seatnow: 0
            })
          }
        }
      })
    }
  },
  cancelBook(e) {
    const that = this;
    var a = that.data.my_seat_id.split("-");
    var x = parseInt(a[0]);
    var y = parseInt(a[1]);
    var col = "color[" + x + "][" + y + "]";
    wx.showModal({
      title: '提示',
      content: '确认取消预约吗？',
      success: function (res) {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "removeMyBook",
            data: {
              openid: that.data.code_openid
            }
          })
          that.setData({
            [col]: 'green',
            temp: 0,
            seatnow: 0,
          });
          wx.cloud.callFunction({
            name: "updateSeatInfo",
            data: {
              openid: that.data.code_openid
            }
          })
          that.onLoad();
          wx.switchTab({
            url: '../my_msg/my_msg',
          });
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  onTabItemTap(item) {
    this.onLoad();
  }
})