const db = wx.cloud.database()
Page({
  data: {
    flag: 0
  },
  jumpToBookRecord(e) {
    wx.navigateTo({
      url: '../book_record/book_record',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  jumpToScan(e) {
    wx.navigateTo({
      url: '../scan/scan',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  jumpToInfo(e) {
    wx.navigateTo({
      url: '../information/information',
      success: (result) => {
        console.log("sucess!");
      },
      fail: () => {
        console.log("fail!");
      },
      complete: () => {
        console.log("finish!");
      }
    });
  },
  onLoad() {
    var that = this;
    wx.cloud.callFunction({
      name: 'get',
    }).then(res => {
      console.log(res.result.openid)
      that.setData({
        code_openid: res.result.openid
      })
      //根据云函数获取我的预约信息
      wx.cloud.callFunction({
        name: "checkMyBookInfo",
        data: {
          openid: that.data.code_openid
        }
      }).then(res => {
        that.setData({
          flag: res.result.data.length
        })
      })
    })
  },
  onTabItemTap(item) {
    this.onLoad();
  }

})