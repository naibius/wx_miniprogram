Page({
  data: {

  },
  //跳转至主页
  jumpToHome(e) {
    wx.switchTab({
      url: '../home/home'
    });
  },
  jumpToHistory(e) {
    wx.navigateTo({
      url: '../book_his/book_his'
    });
  },
  //获取我的预约数据
  onLoad: function (options) {
    var that = this;
    wx.cloud.callFunction({
      name: 'get',
    }).then(res => {
      console.log(res.result.openid)
      that.setData({
        code_openid: res.result.openid
      })
      //根据云函数获取openid等信息
      wx.cloud.callFunction({
        name: "checkMyBookInfo",
        data: {
          openid: that.data.code_openid
        }
      })
        .then(res => {
          that.setData({
            dataList: res.result.data
          })
        })
        .then(res => {
          var a = that.data.dataList[0].seat_id.split("-");
          var x = parseInt(a[0]);
          var y = parseInt(a[1]);
          that.setData({
            row: x + 1,
            column: y + 1
          })
        })
    }
  )}
})
