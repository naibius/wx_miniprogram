// 云函数入口文件 用于book_chair.js,seat_msg.js
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  try{
    return await db.collection("book_record")
    .where({_openid:event.openid})
    .remove()
    .then(res=>{
      console.log('删除'+res.stats.removed);
    })
  }
  catch(e) {
    console.error(e)
  }

}