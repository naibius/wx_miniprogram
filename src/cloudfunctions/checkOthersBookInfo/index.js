// 云函数入口文件 用于book_record.js,seat_msg.js
const cloud = require('wx-server-sdk')
 
cloud.init() 
const db=cloud.database();
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection("book_record")
  .where({_openid:_.neq(event.openid)})
  .get()
}