// 云函数入口文件 用于book_record.js,seat_msg.js
const cloud = require('wx-server-sdk')
 
cloud.init() 
const db=cloud.database();
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  return await db.collection("seat")
  .where({_openid:_.eq(event.openid)})
  .remove()
}